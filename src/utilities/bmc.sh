#!/bin/sh
# Copyright (C) 2018 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

#################
# Configuration #
#################
gpio_names="power reset"
gpio_nums="0 1" # MB_POWER_BTN, MB_RESET_BTN
# unconnected relays: 48 5

get_list_element()
{
    index="$(expr $1 + 1)"
    shift 1
    eval "echo $(printf "$%s" ${index})"
}

get_gpio_num_by_name()
{
    i=0
    if [ -z "${gpio_names}" ] ; then
	echo "Error: gpio.sh: gpio_name variable is empty"
	error=1
    fi

    if [ -z "${gpio_nums}" ] ; then
	echo "Error: gpio.sh: gpio_nums variable is empty"
	error=1
    fi

    for gpio_name in ${gpio_names} ; do
	if [ "${gpio_name}" = "$1" ] ; then
	    echo "$(get_list_element ${i} ${gpio_nums})"
	fi
	i=$(expr $i + 1)
    done
}

usage()
{
    echo "$0 <power|reset> <release|press> # Set relay value"
    echo "$0 <power|reset> # press and release the corresponding button"
    echo "$0 console"
    exit 1
}

relay_state_to_gpio_value()
{
    state="$1"
    if [ "${state}" = "press" ] ; then
	echo 1
    elif [ "${state}" = "release" ] ; then
	echo 0
    else
	echo "relay_state_to_gpio_value: unknown state '$1'"
	exit 1
    fi
}

relay_set()
{
    gpio_num="$(get_gpio_num_by_name $1)"
    gpio_value="$(relay_state_to_gpio_value $2)"
    
    error=0
    if [ -z "${gpio_num}" ] ; then
	echo "Error: relay_set: wrong relay name: '$1'"
	echo "       available names: ${gpio_names}"
	error=1
    fi

    if [ -z "${gpio_value}" ] ; then
	echo "Error: relay_set: wrong relay state: '$2'"
	error=1
    fi

    if [ ${error} -eq 1 ] ;then
	exit 1
    fi

    gpioset host-computer-front-panel "${gpio_num}=${gpio_value}"
}

relay_toggle()
{
    relay_name="$1"
    relay_set ${relay_name} "release"
    sleep 1
    relay_set ${relay_name} "press"
    sleep 1
    relay_set ${relay_name} "release"
    sleep 1
}

if [ $# -eq 1 -a "$1" = "console" ] ; then
    picocom /dev/ttyGS0
elif [ $# -eq 1 ] ; then
    relay_toggle "$1"
elif [ $# -eq 2 ] && [ "$2" = "release" -o "$2" = "press" ] ; then
    relay_set "$1" "$2"
else
    usage
fi
