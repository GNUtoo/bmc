/*
 * Copyright (C) 2021 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libevdev/libevdev.h>

char* button_code_to_name(int code)
{
	/* TODO: The devicetree exports the button name, so it might be possible
	 * to automatically retrieve the name instead and always rely on the 
	 * button name instead of using BTN_0 and BTN_1.
	 */
	switch (code) {
	case BTN_0:
		return "CASE_BTN_POWER";
	case BTN_1:
		return "CASE_BTN_RESET";
	}
}

int main(void) {
	struct libevdev *dev = NULL;
	int fd;
	int rc = 1;

	/* TODO: look for the right device automatically */
	fd = open("/dev/input/event1", O_RDONLY|O_NONBLOCK);
	rc = libevdev_new_from_fd(fd, &dev);
	if (rc < 0) {
		fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
		exit(1);
	}

	if (!libevdev_has_event_type(dev, EV_KEY) ||
	    !libevdev_has_event_code(dev, EV_KEY, BTN_0) ||
	    !libevdev_has_event_code(dev, EV_KEY, BTN_1)) {
		printf("Failed to find the case buttons input device\n");
		exit(1);
	}

	do {
		struct input_event ev;
		rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
		if (rc == 0) {
			/* TODO: 
			 * - print time
			 * - dispatch the actions
			 */
			if (ev.type == EV_KEY) {
				printf("Event: %s: %d\n",
				       button_code_to_name(ev.code), ev.value);
				if (ev.code == BTN_0 && ev.value) {
					system("bmc.sh power press");
				} else if (ev.code == BTN_0) {
					system("bmc.sh power release");
				} else if (ev.code == BTN_1 && ev.value) {
					system("bmc.sh reset press");
				} else if (ev.code == BTN_1) {
					system("bmc.sh reset release");
				} else {
					printf("Event: %s %s %d\n",
					       libevdev_event_type_get_name(ev.type),
					       libevdev_event_code_get_name(ev.type, ev.code),
					       ev.value);
				}
			}
		}
	} while (rc == 1 || rc == 0 || rc == -EAGAIN);
}
