DESTDIR ?= /usr/local

.PHONY: clean help install

all: simple-bmc.dtbo src/dts/am335x-boneblack.dtb src/dts/am335x-bonegreen.dtb

help:
	@echo "Available targets:"
	@echo -e "\tmake install"
	@echo -e "\tmake simple-bmc.dtbo"

install: simple-bmc.dtbo src/dts/am335x-boneblack.dtb src/dts/am335x-bonegreen.dtb
	install                                            -d $(DESTDIR)/usr/bin/
	install src/utilities/bmc.sh                       -t $(DESTDIR)/usr/bin/
	install                                            -d $(DESTDIR)/usr/share/simple-bmc/
	install src/utilities/Makefile                     -t $(DESTDIR)/usr/share/simple-bmc/
	install src/utilities/computer-case-buttons.c      -t $(DESTDIR)/usr/share/simple-bmc/
	install src/utilities/PKGBUILD                     -t $(DESTDIR)/usr/share/simple-bmc/
	install                                            -d $(DESTDIR)/usr/lib/systemd/system/
	install src/systemd/gpio-aggregator.service        -t $(DESTDIR)/usr/lib/systemd/system/
	install src/systemd/buttons.service                -t $(DESTDIR)/usr/lib/systemd/system/
	install                                            -d $(DESTDIR)/etc/modules-load.d/
	install src/modules-load.d/gpio-aggregator.conf    -t $(DESTDIR)/etc/modules-load.d/
	install                                            -d $(DESTDIR)/boot/dtbos/linux-libre/
	install simple-bmc.dtbo                            -t $(DESTDIR)/boot/dtbos/linux-libre/
	install                                            -d $(DESTDIR)/boot/patched-dtbs
	install src/dts/am335x-boneblack.dtb               -t $(DESTDIR)/boot/patched-dtbs
	install src/dts/am335x-bonegreen.dtb               -t $(DESTDIR)/boot/patched-dtbs
	install                                            -d $(DESTDIR)/usr/share/libalpm/hooks/
	install src/pacman-hooks/simple-bmc-install.hook   -t $(DESTDIR)/usr/share/libalpm/hooks/
	install src/pacman-hooks/simple-bmc-remove.hook    -t $(DESTDIR)/usr/share/libalpm/hooks/

	@echo "You can now add \"fdtoverlays <path>\" to /boot/extlinux/extlinux.conf"
	@echo "See doc/README.pxe in u-boot source code for more details"

.simple-bmc.tmp: src/dts/simple-bmc.dts
	cpp -nostdinc -undef -I external/devicetree-rebasing/include/ -o $@ $<

simple-bmc.dtbo: .simple-bmc.tmp
	dtc -@ -I dts -O dtb -o $@ $<

src/dts/am335x-boneblack.dtb: src/dts/am335x-boneblack.dts
	cp $< external/devicetree-rebasing/src/arm/am335x-boneblack-bmc.dts
	make -C external/devicetree-rebasing src/arm/am335x-boneblack-bmc.dtb
	mv external/devicetree-rebasing/src/arm/am335x-boneblack-bmc.dtb $@
	rm -f external/devicetree-rebasing/src/arm/am335x-boneblack-bmc.dts

src/dts/am335x-bonegreen.dtb: src/dts/am335x-bonegreen.dts
	cp $< external/devicetree-rebasing/src/arm/am335x-bonegreen-bmc.dts
	make -C external/devicetree-rebasing src/arm/am335x-bonegreen-bmc.dtb
	mv external/devicetree-rebasing/src/arm/am335x-bonegreen-bmc.dtb $@
	rm -f external/devicetree-rebasing/src/arm/am335x-bonegreen-bmc.dts

clean:
	rm -f .simple-bmc.tmp simple-bmc.dtbo
