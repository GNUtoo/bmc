== TODO ==
- Add support for it in bundlewrap:
  - Add ~GNUtoo as feed in bundlewrap
  - Use fdtoverlays in /boot/extlinux/extlinux.conf

== License ==
All the files are (also) licensed under the GPLv3 or later.
